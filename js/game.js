//Initialize game into 1024x768 canvas. Phaser.AUTO switches to WebGL if possible.
var game = new Phaser.Game(500, 500, Phaser.AUTO, 'game_div');
var music;

//State to preload game data
game.state.add('load', {preload: loader_preload, create: loader_create});

//State to display menu and game
game.state.add('menu', {create: menu_create});
game.state.add('game', {create: game_create, update: game_update});
game.state.add('quit', {create: quit_create});
game.state.add('fail', {create: fail_create});

//Start the game with load state
game.state.start('load');

//Mouse move callback used into menu or game
function mouse_move()
{
	this.cursor_sprite.x = this.game.input.x;
	this.cursor_sprite.y = this.game.input.y;
}


