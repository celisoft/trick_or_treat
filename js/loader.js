//This var will contains the displayed text at the loading screen
var load_status;

function loader_preload()
{
	this.game.load.image('menu_bg', 'assets/menu.png');
}

//Preload data required by the game
function loader_load()
	{
		//Common UI graphics
		game.load.image('cursor_sprite', 'assets/cursor.png');
    	game.load.image('candy1', 'assets/candy1.png');
		game.load.image('candy2', 'assets/candy2.png');
		game.load.image('candy3', 'assets/candy3.png');
    	game.load.image('ennemies1', 'assets/monsters/ennemies1.png');
		game.load.image('ennemies2', 'assets/monsters/ennemies2.png');
		game.load.image('ennemies3', 'assets/monsters/ennemies3.png');
    	game.load.image('bt_play', 'assets/bt_play.png');
		game.load.image('bt_quit', 'assets/bt_quit.png');
	
		//Background music
		game.load.audio('music', ['assets/sfx/mp3/music.mp3', 'assets/sfx/ogg/music.ogg']);
	
		this.game.load.image('game_bg', 'assets/background.png');
		this.game.load.image('pic_fail', 'assets/pic_fail.png');

		this.game.load.start();
	}

//Display loading text and launch preload after defining what to do next
function loader_create()
	{
		this.game.add.image(0, 0, 'menu_bg');

		this.game.load.onFileComplete.add(loader_refresh, this);
		this.game.load.onLoadComplete.add(loader_next, this);

		//Display the status of file loading to inform the user that it's in progress
		load_status = this.game.add.text(this.game.world.width/2, this.game.world.height-32, 'Preloading data', {fontSize: '16px', fill: '#000000'});

		loader_load();
	}

function loader_refresh(progress, cacheKey, success, totalLoaded, totalFiles)
	{
		load_status.setText("[" + cacheKey + "]");
	}

//Swich to menu state
function loader_next()
{
	this.game.state.start('menu');
}
