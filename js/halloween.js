var cursor_sprite;
var ennemies;
var candies;
var ennemies_list = ['ennemies1','ennemies2', 'ennemies3'];
var candies_list = ['candy1','candy2','candy3'];
var coord_ennemies = [0, 64, 128, 436, 500, 510];
var coord_candies = [ 64, 128, 192, 200, 250, 372, 400, 436];
var factor = 1;
var timer;
var total = 0;
var logo;
var text;
var text_click;
var can_click = false;
var count;
var tween;
var method = 0;
var initial_width = 0;
var initial_height = 0;
var margin = 0;

function game_create() {

    game.physics.startSystem(Phaser.Physics.ARCADE);
	// we need to add margin to the world, so the camera can move
  	margin = 50;
  	initial_width = game.world.width;
  	initial_height = game.world.height;
  	// and set the world's bounds according to the given margin
  	var x = -margin;
 	var y = -margin;
  	var w = game.world.width + margin * 2;
  	var h = game.world.height + margin * 2;
  	// it's not necessary to increase height, we do it to keep uniformity
  	game.world.setBounds(x, y, w, h);
  
  	// we make sure camera is at position (0,0)
  	game.world.camera.position.set(0);
	
	//background  
	game.add.image(0, 0, 'game_bg');
	//timer used to determinate when regenerate sprites and allow the click to destroy monsters
	timer = game.time.create(false);
    timer.loop(1500, regenerate, this);
	timer.loop(20000, allow_click, this);
    timer.start();
    //Here's where you find cursor_sprite params which will follow the cursor
	cursor_sprite = game.add.sprite(game.world.centerX, game.world.centerY, 'cursor_sprite');
    game.input.addMoveCallback(mouse_move);

    //gonna be used with physics... 
    game.physics.enable( cursor_sprite, Phaser.Physics.ARCADE);
	
	//and will stay in the game!
	cursor_sprite.body.collideWorldBounds=true;
	
	//intialize scoring
	count = 0;
	text = game.add.text(378, 32, "Score", {
        font: "15px Arial",
        fill: "#ff6a00",
    });
	
	//intialize candies in a group
	candies = game.add.group();
	//loop to create 4 candies
	for(var can = 0; can<=3; can++)
    {	
		//There will be 8 differents random coord...
		var canX = game.rnd.integerInRange(0, 7);
		var canY = game.rnd.integerInRange(0, 7);
		//and 3 differents colors!
		var canName = game.rnd.integerInRange(0, 2);
		//This is what a called a candy!
		var candy = candies.create(coord_candies[canX], coord_candies[canY] , candies_list[canName]);
		//In the right position...
		candy.anchor.set(0.5);
		//and ready to used.
		game.physics.enable(candies, Phaser.Physics.ARCADE);
		//Tweening
		tween = game.add.tween(candies).to( { x: [ w, w, 0, 0 ], y: [ 0, h, h, 0 ] }, 4000, "Sine.easeInOut", true, -1, false);
		tween.onLoop.add(changeMethod, this);
    }
	
	//initalize ennemies like we did with candies
	ennemies = game.add.group();
	for(var enn = 0; enn<=2; enn++)
    {
		var ennX = game.rnd.integerInRange(0, 5);
		var ennY = game.rnd.integerInRange(0, 5);
		var ennName = game.rnd.integerInRange(0, 2);
		var ennemy = ennemies.create(coord_ennemies[ennX], coord_ennemies[ennY] , ennemies_list[ennName]);
		ennemy.anchor.set(0.5);
		game.physics.enable(ennemies, Phaser.Physics.ARCADE);
    }
	
 }
 
function game_update() {    
	//Check some possibilities(obj1, obj2, function called)
	game.physics.arcade.overlap(cursor_sprite, ennemies, haunting);
	game.physics.arcade.overlap(cursor_sprite, candies, eating);
	//loop to destroy ennemies
	if (can_click == true)
	{
		if (game.input.activePointer.isDown)
    	{
		createQuake();
		ennemies.forEachAlive(kill_ennemies, this);
		can_click = false;
		text_click.destroy();
		}
	}
	//loop to initialize monsters movements
	if (game.input.mousePointer.active)
    {
       
    	ennemies.forEach(game.physics.arcade.moveToPointer, game.physics.arcade, false, 10);

    }
    else
    {
    	ennemies.setAll('body.velocity.x', 0);
        ennemies.setAll('body.velocity.y', 0);
    }
}
// Tweenings methods for candies movements  
function changeMethod() {

    method++;

    if (method === 1)
    {
        tween.interpolation(Phaser.Math.bezierInterpolation);
    }
    else if (method === 2)
    {
        tween.interpolation(Phaser.Math.catmullRomInterpolation);
    }
    else if (method === 3)
    {
        method = 0;
        tween.interpolation(Phaser.Math.linearInterpolation);
    }

}
//Regeneration of monsters
function regenerate() {
    total++;
	var ennX = game.rnd.integerInRange(0, 7);
	var ennY = game.rnd.integerInRange(0, 7);
	var ennName = game.rnd.integerInRange(0, 2);
	var ennemy = ennemies.create(coord_ennemies[ennX], coord_ennemies[ennY] , ennemies_list[ennName]);
	ennemy.anchor.set(0.5);
	game.physics.enable(ennemies, Phaser.Physics.ARCADE);	
	}
	
// After a moment you will able to destroy monsters by clicking
function allow_click() {
	can_click = true;
	text_click = game.add.text(170, 124, "Click !", {
	font: "45px Arial",
	fill: "#007d89",
		});
	}
	
//Easy to understand
function kill_ennemies(e){
	e.kill();
	}
	
function createQuake() { 
  // define the camera offset for the quake
  var rumbleOffset = 10;
  // we need to move according to the camera's current position
  var properties = {
    x: game.camera.x - rumbleOffset
  	};
  // we make it a relly fast movement
  var duration = 100;
  // because it will repeat
  var repeat = 2;
  // we use bounce in-out to soften it a little bit
  var ease = Phaser.Easing.Bounce.InOut;
  var autoStart = false;
  // a little delay because we will run it indefinitely
  var delay = 0;
  // we want to go back to the original position
  var yoyo = true;
  
  var quake = game.add.tween(game.camera)
    .to(properties, duration, ease, autoStart, delay, 4, yoyo);
  
  // let the earthquake begins
  quake.start();
  
}



//cursor sprite vs candies 
function eating(c_s, can){
	can.kill(); 
	updateText();
	var canX = game.rnd.integerInRange(0, 7);
	var canY = game.rnd.integerInRange(0, 7);
	var canName = game.rnd.integerInRange(0, 2);
	var candy = candies.create(coord_candies[canX], coord_candies[canY] , candies_list[canName]);
	candy.anchor.set(0.5);
	game.physics.enable(candy, Phaser.Physics.ARCADE);
	}

//ennemies vs cursor sprite		
function haunting(enn, cur){
	cur.kill();
	game.state.start('fail');
	}
	
function updateText() {
	count++;
	text.setText("Score =" + count);
	}

function back_to_menu(){
	game.world.setBounds(0, 0, initial_width, initial_height);
	this.game.state.start('menu');
	}

//Function to replace current location by the official game page
function goto_celisoft(){
	window.location.replace('http://www.celisoft.com/');
	}
	

