# Introduction #

Trick or Treat is an Halloween little game designed for smartphones and tablets but also playable with your mouse's computer. Collect candies and avoid ghosts until you can click to clean your area.

This game has been created by Zoé Belleton in 2015 under GPL v3 licence.